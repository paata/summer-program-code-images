import os

import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib
matplotlib.use('cairo')
import matplotlib.pyplot as plt
import pickle
import utils
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors
import matplotlib.colors as mcolors
import matplotlib.cm as cmc


ifile = 'inun_minremoved_v1v2.pkl'
pfile = 'delta_3B42_precip.pkl'
datadir = 'data'
delta = 'Ganges'
p_data = utils.get_data(datadir, pfile, delta)
precip_data = p_data['data']
i_data = utils.get_data(datadir, ifile, delta)
inun_data = i_data['data']

############################################################
def frange(start, end=None, inc=None):
    "A range function, that does accept float increments..."

    if end == None:
        end = start + 0.0
        start = 0.0

    if inc == None:
        inc = 1.0

    L = []
    while 1:
        next = start + len(L) * inc
        if inc > 0 and next >= end:
            break
        elif inc < 0 and next <= end:
            break
        L.append(next)
        
    return L

############################################################


 


corr = []

for spam in range(1,151):
	precip = pd.ewma(precip_data.mean(axis=1), span=spam)
	end = min(precip.index[-1], inun_data.index[-1])
	iclip = inun_data.mean(axis=1)[:end] 
	pclip = precip[iclip.index[0]:end:10]
	mask = (np.isfinite(iclip) & np.isfinite(pclip))
	corr.append(st.pearsonr(iclip[mask],pclip[mask])[0])
print corr
################################################################

#it will save corr matrix in numpy so creating it again is no longer necessary
np.save("correlationemwa", corr)
corr = np.load('correlationemwa.npy', mmap_mode='r')
#corr.max() same as np.max(corr) returns the biggest value
#np.argmax(corr) returns the location
max_corr = corr.max()

inds = np.argmax(corr)
print max_corr
print inds
#lam_bda = 1.0*2/(inds+1) 

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(range(1,151),corr)
ax.set_title("EMWA Analysis\nMaximum Correlation is: {} When Span is: {}".format(max_corr,inds) )
ax.set_ylim(0,1)
ax.set_xlabel("Span")
ax.set_ylabel("Correlation")
plt.savefig('correlation_EWMA_graph_Ganges.png')

