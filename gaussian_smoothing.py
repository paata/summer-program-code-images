import os

import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib
matplotlib.use('cairo')
import matplotlib.pyplot as plt
import pickle
import utils
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors
import matplotlib.colors as mcolors
import matplotlib.cm as cmc


ifile = 'inun_minremoved_v1v2.pkl'
pfile = 'delta_3B42_precip.pkl'
datadir = 'data'
delta = 'Mekong'
p_data = utils.get_data(datadir, pfile, delta)
precip_data = p_data['data']
i_data = utils.get_data(datadir, ifile, delta)
inun_data = i_data['data']



############################################################
#corr = [[np.nan for x in xrange(181)] for x in xrange(181)] 

 
windows = 180
sigmas = 180

corr = np.empty((windows,sigmas)) * np.nan

for wind in range(1,windows+1):
	for std in range(1,sigmas+1):
		precip = pd.rolling_window(precip_data.mean(axis=1),
	window=wind, win_type='gaussian', std=std)
		end = min(precip.index[-1], inun_data.index[-1])
		iclip1 = inun_data.mean(axis=1)[:end] 
		pclip1 = precip[iclip1.index[0]:end:10]
		mask = (np.isfinite(iclip1) & np.isfinite(pclip1))
		corr[wind-1][std-1] = st.pearsonr(iclip1[mask],pclip1[mask])[0]

################################################################

#it will save corr matrix in numpy so creating it again is no longer necessary
#np.save("correlation", corr)
#corr = np.load('correlation.npy', mmap_mode='r')
#corr.max() same as np.max(corr) returns the biggest value
#np.argmax(corr) returns the location
max_corr = corr.max()
inds = np.unravel_index(np.argmax(corr), (windows, sigmas))


cmap = cmc.Blues          #Figure this out later
fig1 = plt.figure()
ax3 = fig1.add_subplot(1,1,1)
ax3.set_title("(Mekong) Maximum Correlation is: {} Located at: {}  ".format(max_corr,inds) )
image = ax3.matshow(corr,cmap='copper' , norm = mcolors.BoundaryNorm(np.arange(0,1.1,.1), cmap.N) ) 
cbar = fig1.colorbar(image, ax = ax3,fraction=0.045, cmap='copper')
cbar.set_label("Correlation")
ax3.set_xlabel("Standard Deviation")
ax3.set_ylabel("Window")
plt.savefig('correlation_graph_Ganges.png')


