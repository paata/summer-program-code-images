import os
import sys
import matplotlib.colors as mcolors
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib
#matplotlib.use('cairo')
import matplotlib.pyplot as plt
import pickle
import utils
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors


#dpath = 'delta_hires'
ifile = 'inun_minremoved_v1v2.pkl'
pfile = 'delta_3B42_precip.pkl'
datadir = 'data'
delta = 'Amazon'
p_data = utils.get_data(datadir, pfile, delta)
precip_data = p_data['data']
i_data = utils.get_data(datadir, ifile, delta)
inun_data = i_data['data']

 
precip = pd.rolling_window(precip_data,
			window=45, win_type='gaussian', std=39)
#mask = (np.isfinite(itm) & np.isfinite(pmtm))
#slope, intercept, _, _, _ = st.linregress(itm[mask], pmtm[mask])


fig = plt.figure()
ax = fig.add_subplot(1,1,1)





#ax.scatter(itm[mask], pmtm[mask])
#ax.plot(itm, slope*itm + intercept, color='k')
#ax.set_title("Correlation: {:.3f}".format(st.pearsonr(itm[mask], pmtm[mask])[0]))`	
end = min(precip.index[-1], inun_data.index[-1])
iclip = inun_data[:end] 
pclip = precip[inun_data.index[0]:end:10]
mask = (np.isfinite(iclip) & np.isfinite(pclip))
corrs = []

for i in range(iclip.shape[1]):
    mask = (np.isfinite(pclip.iloc[:,i]) & np.isfinite(iclip.iloc[:,i]))
    corrs.append(st.pearsonr(pclip.iloc[:,i][mask],iclip.iloc[:,i][mask])[0])

print len(corrs)
deltaname = 'Amazon'
title='Map of Correlations Using Gaussian \nRolling Window'
print min(corrs)
print max(corrs)
cmap = 'RdBu'
vmax = 1
vmin = -1
norm = colors.Normalize(vmin=vmin, vmax=vmax)
globalMap = utils.gridEdges(datadir)
# norm = colors.Normalize(vmin=vmin, vmax=vm
utils.plotGrid(ax,corrs, p_data, globalMap, deltaname, title, cmap, norm)

# ax.set_title("Ten Day Correlation")

plt.savefig('corr_by_10day_Amazon.png')


plt.close()
