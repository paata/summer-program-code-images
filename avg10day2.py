import os
import sys
import matplotlib.colors as mcolors
import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib
#matplotlib.use('cairo')
import matplotlib.pyplot as plt
import pickle
import utils
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors


#dpath = 'delta_hires'


ifile = 'Ganges_inun_clip.pkl'
pfile = 'Ganges_precip_10D_avg.pkl'
pfile1 = 'delta_3B42_precip.pkl'
datadir = 'data'
delta = 'Ganges'
pclip = pd.read_pickle(pfile)
print pclip.shape
#pclip = p_data['data']
iclip = pd.read_pickle(ifile)
print iclip.shape
#iclip = i_data['data']



#mask = (np.isfinite(itm) & np.isfinite(pmtm))
#slope, intercept, _, _, _ = st.linregress(itm[mask], pmtm[mask])



fig = plt.figure()
ax = fig.add_subplot(1,1,1)





#ax.scatter(itm[mask], pmtm[mask])
#ax.plot(itm, slope*itm + intercept, color='k')
#ax.set_title("Correlation: {:.3f}".format(st.pearsonr(itm[mask], pmtm[mask])[0]))`	


corrs = []
print iclip.shape
print pclip.shape

for i in range(iclip.shape[1]):
    mask = (np.isfinite(pclip.iloc[:,i]) & np.isfinite(iclip.iloc[:,i]))
    corrs.append(st.pearsonr(pclip.iloc[:,i][mask],iclip[mask].iloc[:,i][mask])[0])

print len(corrs)

title='Map of Correlation by 10 Days (Mean)'

cmap = 'RdBu'
vmax = 1
vmin = -1
norm = colors.Normalize(vmin=vmin, vmax=vmax)
globalMap = utils.gridEdges(datadir)
p_data = utils.get_data(datadir, pfile1, delta)

# norm = colors.Normalize(vmin=vmin, vmax=vmax)
utils.plotGrid(ax,corrs, p_data, globalMap, delta, title, cmap, norm)

# ax.set_title("Ten Day Correlation")

plt.savefig('corr_by_10day2_Mean_Ganges.png')


plt.close()
