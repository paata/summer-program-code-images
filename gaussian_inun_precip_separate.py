import os

import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib
matplotlib.use('cairo')
import matplotlib.pyplot as plt
import pickle
import utils
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors
import matplotlib.colors as mcolors
import matplotlib.cm as cmc


ifile = 'inun_minremoved_v1v2.pkl'
pfile = 'delta_3B42_precip.pkl'
datadir = 'data'
delta = 'Ganges'
p_data = utils.get_data(datadir, pfile, delta)
precip_data = p_data['data']
i_data = utils.get_data(datadir, ifile, delta)
inun_data = i_data['data']



def get_std_list():
	corrolations1 = []
	for deviation in range(1,181):
		precip = pd.rolling_window(precip_data.mean(axis=1),
			window=45, win_type='gaussian', std=deviation)
		end = min(precip.index[-1], inun_data.index[-1])
		iclip = inun_data.mean(axis=1)[:end] 
		pclip = precip[iclip.index[0]:end:10]
		np.testing.assert_array_equal(pclip.index, iclip.index)

	
		mask = (np.isfinite(iclip) & np.isfinite(pclip))
		
		
		corrolations1.append(st.pearsonr(iclip[mask],pclip[mask])[0])
	return corrolations1
		



std_corrolations = get_std_list()

window_corrolations = []
for w in range(1,51):
	  #gaussian smoothing
	precip = pd.rolling_window(precip_data.mean(axis=1),
	window=w, win_type='gaussian', std=39)
	  #put on 10 year time scale matched to inudation
	end = min(precip.index[-1], inun_data.index[-1])
	iclip = inun_data.mean(axis=1)[:end] 
	pclip = precip[iclip.index[0]:end:10]
	np.testing.assert_array_equal(pclip.index, iclip.index)
	mask = (np.isfinite(iclip) & np.isfinite(pclip))
	
	window_corrolations.append(st.pearsonr(iclip[mask],pclip[mask])[0])

	#print corrs
fig = plt.figure()
ax = fig.add_subplot(2,1,1)
ax2 = fig.add_subplot(2,1,2)
  #print corrolations
  #print len(corrolations)
print type(window_corrolations)
ax.plot(range(len(window_corrolations)),window_corrolations)
ax.set_xlim(0,51)
ax.set_ylim(0,1)
ax.set_title("Shifting Window Size With Sigma of 39")
ax.set_ylabel("Correlation")
ax2.plot(range(len(std_corrolations)),std_corrolations)
ax2.set_xlim(0,181)
ax2.set_ylim(0,1)
ax2.set_title("Shifting Sigma With Window Size of 45")
ax2.set_ylabel("Correlation")

plt.savefig('inun_and_precip_corrT.png')
