import os
import numpy as np
import matplotlib.pyplot as plt
import pickle
from mpl_toolkits.basemap import Basemap, cm
import matplotlib.animation as animation
from matplotlib import colors

import utils
import PCAmekong
 
datadir = 'data'
datafile = 'delta_3B42_precip.pkl'
deltaName = 'Ganges'

def assign(rowcol,functiontype,delta):
    results = []
    if (rowcol == 1):
        if(functiontype=="mean"):
            results=delta.mean(axis=1)
        if(functiontype=="sum"):
            results=delta.sum(axis=1)
        if(functiontype=="deviatiom"):
            results=delta.std(axis=1)
    if (rowcol == 0):
        if(functiontype=="mean"):
            results=delta.mean(axis=0)
        if(functiontype=="sum"):
            results=delta.sum(axis=0)
        if(functiontype=="deviatiom"):
            results=delta.std(axis=0)
    return results




delta = utils.get_data(datadir, datafile, deltaName)
fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

globe = utils.gridEdges(datadir)
results = assign(0,"mean",delta['data']);
cmap = cm.GMT_drywet
vmin = np.nanmin(results.values)
vmax = np.nanmax(results.values)
norm = colors.Normalize(vmin=vmin, vmax=vmax)
bm = utils.basemap(ax1)
X, Y = bm(globe['lons'], globe['lats'])

ax1.axis(utils.mapbounds[deltaName])
globe['map'][delta['inds'][0], delta['inds'][1]] = results


im = bm.pcolormesh(X, Y, np.ma.masked_invalid(globe['map']),cmap=cmap, norm=norm)
cbar = bm.colorbar(im, "bottom", cmap=cmap, norm=norm)  
plt.show()